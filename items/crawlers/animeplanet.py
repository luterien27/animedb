

website = "http://www.anime-planet.com/"
website_key = "anime-planet"

anime_list_url = "http://www.anime-planet.com/anime/all"
anime_detail_url_template = "http://www.anime-planet.com/anime/%(slug)"


def anime_list():
    """

        anime listesini çek, her bir animeyi AnimeResource olarak kaydet

    """
    pass



def anime_details():
    """

        AnimeResource nesnelerini getir, her resource için url'den
        verileri al, json olarak kaydet

    """
    pass


def resource_to_anime():
    """

        Tamamlanmış AnimeResource nesnelerini
        Anime nesnesine çevir, verileri kaydet

    """
    pass

