from __future__ import unicode_literals

from django.db import models


# durum seçenekleri;
# yayinda, bitti, henuz yayinlanmadi

STATUS_CHOICES = (
    ('airing', 'Currently Airing'),
    ('complete', 'Complete'),
    ('not_yet_aired', 'Not yet aired'),
)

TYPE_CHOICES = (
    ('tv', 'TV'),
    ('movie', 'Movie'),
)

class Anime(models.Model):
    """ 

        şuan için gerekli alanlar eklendi

        - parent;

            iki sezonluk bir animede, ilk sezon ikincinin parentı oluyor

        - duration

            bölüm başı süre
            format => saat:dakika 
            1 saat 25 dakikalık bir film için; 1:25
            24 dakikalık bir anime için; 0:24
            olacak

        - episodes

            bölüm sayısı

        - anime_type

            normal animeler için tv, filmler için movie

    """
    title = models.CharField(max_length=120, 
        null=True, blank=True)
    slug = models.SlugField()

    anime_type = models.CharField(max_length=30, null=True, 
        blank=True, choices=TYPE_CHOICES)

    original_title = models.CharField(max_length=120, 
        null=True, blank=True)

    date_released = models.CharField(max_length=30, 
        null=True, blank=True)

    episodes = models.IntegerField(null=True, blank=True)

    duration = models.CharField(max_length=10, 
        null=True, blank=True)

    status = models.CharField(max_length=30, null=True, 
        blank=True, choices=STATUS_CHOICES)

    description = models.TextField()

    parent = models.ForeignKey('self', null=True, blank=True)

    image = models.ImageField(upload_to="animes/images/preview", 
        null=True, blank=True)

    studios = models.ManyToManyField('Studio', 
        related_name="animes")

    related_animes = models.ManyToManyField('self', 
        related_name="related")

    class Meta:
        pass

    def __unicode__(self):
        return u"%s" % self.title

    def get_related_animes(self):
        pass


class Tag(models.Model):
    """ etiketler, varsa ekleriz """
    title = models.CharField(max_length=120, null=True, blank=True)
    slug = models.SlugField()


class Genre(models.Model):
    """ tür; macera, aksiyon, komedi vs """
    title = models.CharField(max_length=120, null=True, blank=True)
    slug = models.SlugField()


class Studio(models.Model):
    """ animeyi yapan studyo """
    title = models.CharField(max_length=120, null=True, blank=True)
    slug = models.SlugField()

